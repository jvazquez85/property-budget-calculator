include .env.development
DOCKER := $(shell which docker)
BUILD_ARG = $(if $(filter  $(NOCACHE), 1),--no-cache)
BUILD_ARG_BASE = $(if $(filter $(NOCACHEBASE), 1),--no-cache)
USERID := $(shell id -u)
GROUPID := $(shell id -g)
BASE_PATH := $(shell pwd)
ENV_FILE := .env.development
ENV_FILE ?= $(ENV_FILE)
DOCKER_BUILDKIT := 1
DOCKER_IMAGE_NAME := property-budget-calculator-api
NETWORK_NAME := property-budget-calculator-network
# Publisher defaults
.PHONY: service

all: network

network:
	$(DOCKER) network inspect $(NETWORK_NAME) > /dev/null 2>&1 || $(DOCKER) network create $(NETWORK_NAME)
service:
	sed -e 's/USERNAME/$(GITLAB_USER)/g' -e 's/APIKEY/$(GITLAB_TOKEN)/g' netrc.template > .netrc && \
	$(DOCKER) build $(BUILD_ARG) -f Dockerfile \
		--build-arg build_user_id=$(USERID) \
		--build-arg build_group_id=$(GROUPID) \
		--build-arg build_username=$(BUILD_USERNAME) \
		--build-arg goprivate=$(GOPRIVATE) \
		--build-arg gonoproxy=$(GONOPROXY) \
		--build-arg gonosumdb=$(GONOSUMDB) \
		--build-arg gitlab_token=$(GITLAB_TOKEN) \
		--build-arg gitlab_user=$(GITLAB_USER) \
		-t $(DOCKER_IMAGE_NAME) .
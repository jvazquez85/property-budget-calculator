const babelJest = require('babel-jest');

module.exports = babelJest.createTransformer({
    presets: ['@babel/preset-env', '@babel/preset-typescript'],
    plugins: [
        ['babel-plugin-transform-replace-expressions', {
            replace: [{
                identifierName: 'import.meta.env',
                replacement: {
                    type: 'identifier',
                    value: '{}'
                }
            }]
        }]
    ]
});

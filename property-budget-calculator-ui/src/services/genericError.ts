export interface GenericError {
    message: string;
    response?: {
        data?: { message?: string };
        status?: number;
    };
}
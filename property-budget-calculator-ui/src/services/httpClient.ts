export interface HttpClient {
    post<T>(url: string, data: any): Promise<{ status: number; data: T }>;
    get<T>(url: string, config?: any): Promise<{ status: number; data: T }>;
    put<T>(url: string, data: any): Promise<{ status: number; data: T }>;
    delete<T>(url: string): Promise<{ status: number; data: T }>;
    patch<T>(url: string, data: any): Promise<{ status: number; data: T }>;
}

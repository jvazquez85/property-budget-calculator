import { AxiosHttpClient } from './axiosHttpClient';
import {BaseUpdateService} from "./base/baseUpdateService";
import { HttpClient } from './httpClient';

type Constructor<T, U> = new (service: BaseUpdateService<T>) => U;
export function updateService<T, U>(ServiceClass: Constructor<T, U>, apiUrl: string): U {
        /**
         * Provide the object that we use on the components when we update an entity.
         * All the create operations follow the same pattern, so it's pretty much the same
         * for all of them.
         * Example:
         * const applicationHistoriesCreateService = updateService<ApiRecruiterPaginatedResponse>(
         *     ApplicationHistoriesUpdateService,
         *     apiConfig.API_APPLICATION_HISTORIES
         * );
         */

    const httpClient: HttpClient = new AxiosHttpClient(apiUrl);
    const baseUpdateService: BaseUpdateService<T> = new BaseUpdateService<T>(apiUrl, httpClient);
    return new ServiceClass(baseUpdateService);
}

import { AxiosHttpClient } from './axiosHttpClient';
import { BaseCreateService } from './base/baseCreateService';
import { HttpClient } from './httpClient';

type Constructor<T, U> = new (service: BaseCreateService<T>) => U;
export function createService<T, U>(ServiceClass: Constructor<T, U>, apiUrl: string): U {
    /**
     * Provide the object that we use on the components when we create an entity.
     * All the create operations follow the same pattern, so it's pretty much the same
     * for all of them.
     * Example:
     * const applicationHistoriesCreateService = createService<ApiRecruiterPaginatedResponse>(
     *     BudgetCalculatorCreate,
     *     apiConfig.API_APPLICATION_HISTORIES
     * );
     */
    const httpClient: HttpClient = new AxiosHttpClient(apiUrl);
    const baseCreateService: BaseCreateService<T> = new BaseCreateService<T>(apiUrl, httpClient);
    return new ServiceClass(baseCreateService);
}

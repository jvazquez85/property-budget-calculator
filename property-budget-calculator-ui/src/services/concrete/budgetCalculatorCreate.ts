import { BaseCreateService } from '../base/baseCreateService';
import apiConfig from "../../apiConfig";
import {BudgetCalculatorWriteRequest} from "../../interfaces/BudgetCalculatorResponse.ts";
// import {logInfo} from "../loggingService";

export class BudgetCalculatorCreate<CombinedResponse> {
    protected apiUrl: string = `${apiConfig.API_CALCULATE}`;
    private createService: BaseCreateService<CombinedResponse>;

    constructor(service: BaseCreateService<CombinedResponse>) {
        this.createService = service;
    }

    async createBudget(data: BudgetCalculatorWriteRequest): Promise<CombinedResponse> {
        // await logInfo('Sending this to budget create', data);
        return await this.createService.createEntity(data);
    }
}

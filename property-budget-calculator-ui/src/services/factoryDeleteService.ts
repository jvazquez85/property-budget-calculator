import { AxiosHttpClient } from './axiosHttpClient';
import { BaseDeleteService } from './base/baseDeleteService';
import { HttpClient } from './httpClient';

type Constructor<U> = new (service: BaseDeleteService) => U;

export function deleteService<U>(ServiceClass: Constructor<U>, apiUrl: string): U {
    /**
     * Provide the object that we use on the components when we update an entity.
     * All the delete operations follow the same pattern, so it's pretty much the same
     * for all of them.
     * Example:
     * const applicationHistoriesDeleteService = deleteService(
     *     ApplicationHistoriesDeleteService,
     *     apiConfig.API_APPLICATION_HISTORIES
     * );
     */
    const httpClient: HttpClient = new AxiosHttpClient(apiUrl);
    const baseDeleteService: BaseDeleteService = new BaseDeleteService(apiUrl, httpClient);
    return new ServiceClass(baseDeleteService);
}
import {LogData} from "../interfaces/logData";
import axios from "axios";
import apiConfig from "../apiConfig";

const log = async (logData: LogData): Promise<void> => {
    try {
        await axios.post(apiConfig.API_LOG, logData);
    } catch (error) {
        console.error('Failed to send log:', error);
    }
};

export const logDebug = (message: string, extra?: Record<string, any>) => log({ level: 'debug', message, extra });
export const logInfo = (message: string, extra?: Record<string, any>) => log({ level: 'info', message, extra });
export const logWarning = (message: string, extra?: Record<string, any>) => log({ level: 'warning', message, extra });
export const logError = (message: string, extra?: Record<string, any>) => log({ level: 'error', message, extra });
export const logCritical = (message: string, extra?: Record<string, any>) => log({ level: 'critical', message, extra });
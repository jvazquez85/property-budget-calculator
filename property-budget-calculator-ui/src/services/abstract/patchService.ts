import {ApiBaseService} from "./apiBaseService.ts";
import {HttpClient} from "../httpClient.ts";

export abstract class PatchService extends ApiBaseService {
    protected abstract getApiUrl(): string;
    protected abstract getHttpClient(): HttpClient;
}
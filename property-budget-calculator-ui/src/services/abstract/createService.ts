import { HttpClient } from '../httpClient';
import {ApiBaseService} from "./apiBaseService.ts";

export abstract class CreateService<CreateResponse> extends ApiBaseService{
    protected abstract getApiUrl(): string;

    protected abstract getHttpClient(): HttpClient;

    abstract createEntity(data: any): Promise<CreateResponse>;
}

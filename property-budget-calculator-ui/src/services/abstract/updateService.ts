import {ApiBaseService} from "./apiBaseService";
import {HttpClient} from "../httpClient";

export abstract class UpdateService extends ApiBaseService {
    protected abstract getApiUrl(): string;
    protected abstract getHttpClient(): HttpClient;
}
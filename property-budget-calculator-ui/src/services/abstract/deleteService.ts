import { HttpClient } from '../httpClient';
import {ApiDeleteResponse} from "../../interfaces/ApiResponse.ts";

export abstract class DeleteService {
    protected abstract getApiUrl(): string;
    protected abstract getHttpClient(): HttpClient;
    abstract deleteEntity(id: number): Promise<ApiDeleteResponse>
}

import {ApiBaseService} from "./apiBaseService";
import { HttpClient } from '../httpClient';

export abstract class RetrieveService extends ApiBaseService {
    protected abstract getApiUrl(): string;
    protected abstract getHttpClient(): HttpClient;
}
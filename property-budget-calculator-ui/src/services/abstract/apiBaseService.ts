import { HttpClient } from '../httpClient';
import { handleApiErrorService } from '../../utils/messages';

export abstract class ApiBaseService {
    protected abstract apiUrl: string;

    protected abstract getHttpClient(): HttpClient;

    protected handleApiError(exception: any, message: string): any {
        return handleApiErrorService(exception, message);
    }
}

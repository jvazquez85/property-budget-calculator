import axios, { AxiosInstance } from 'axios';
import { HttpClient } from './httpClient';
import qs from 'qs';
import router from "../router";
import {eventBus} from "../bus";

export class AxiosHttpClient implements HttpClient {
    private instance: AxiosInstance;

    constructor(baseURL: string) {
        this.instance = axios.create({
            baseURL,
            headers: {
                'Content-Type': 'application/json',
            },
            paramsSerializer: params => qs.stringify(params, { arrayFormat: 'brackets' })
        });
        // this.setAccessToken();
        // this.instance.interceptors.response.use(
        //     response => response,
        //     this.handleTokenExpiration.bind(this)
        // );
    }
    // @ts-ignore
    private setAccessToken() {
        const accessToken = sessionStorage.getItem('access_token');
        if (accessToken) {
            this.instance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
        } else {
            console.log("Access token seems to be empty");
        }
    }
    // @ts-ignore
    private handleTokenExpiration(error: any) {
        if (error.response && error.response.status === 401) {
            console.log("Token expired or invalid, redirecting to login page...");
            sessionStorage.removeItem('access_token');
            // Assuming eventBus and router are available globally
            eventBus.emit('userLoggedOut');
            router.push('/');
        }
        return Promise.reject(error);
    }

    async post<T>(url: string, data: any): Promise<{ status: number; data: T }> {
        const response = await this.instance.post<T>(url, data);
        return { status: response.status, data: response.data };
    }

    async get<T>(url: string, config?: any): Promise<{ status: number; data: T }> {
        const response = await this.instance.get<T>(url, config);
        return { status: response.status, data: response.data };
    }

    async put<T>(url: string, data: any): Promise<{ status: number; data: T }> {
        const response = await this.instance.put<T>(url, data);
        return { status: response.status, data: response.data };
    }

    async delete<T>(url: string): Promise<{ status: number; data: T }> {
        const response = await this.instance.delete<T>(url);
        return { status: response.status, data: response.data };
    }

    async patch<T>(url: string, data: any): Promise<{ status: number; data: T }> {
        const response = await this.instance.patch<T>(url, data);
        return { status: response.status, data: response.data };
    }
}

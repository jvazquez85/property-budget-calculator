import { StatusCodes } from 'http-status-codes';
import { PatchService } from '../abstract/patchService';
import { HttpClient } from '../httpClient';
import {handleApiErrorService} from "../../utils/messages.ts";
import {ApiError, ApiPatchResponse} from "../../interfaces/ApiResponse.ts";

export class BasePatchService extends PatchService {
    private httpClient: HttpClient;
    protected apiUrl: string;

    constructor(apiUrl: string, httpClient: HttpClient) {
        super();
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    protected getApiUrl(): string {
        return this.apiUrl;
    }

    protected getHttpClient(): HttpClient {
        return this.httpClient;
    }

    async patchEntity(data: Partial<any>, id: number): Promise<ApiPatchResponse> {
        /**
         * Performs a patch operation with any given data to the provided id
         */
        try {
            const httpClient = this.getHttpClient();
            const url = `${this.getApiUrl()}/${id}`;
            const response = await httpClient.patch<void>(url, data);
            if (response.status === StatusCodes.NO_CONTENT) {
                return { success: true, message: "Patch operation succeeded" };
            } else {
                throw new Error(`Unexpected status code: ${response.status}`);
            }
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error patching the entity');
        }
    }

    protected handleApiError(exception: any, message: string): ApiError {
        return handleApiErrorService(exception, message);
    }
}

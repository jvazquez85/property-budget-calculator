import { StatusCodes } from 'http-status-codes';
import { RetrieveService } from '../abstract/retrieveService';
import { HttpClient } from '../httpClient';
import { FIRST_PAGE, RECORDS_PER_PAGE } from '../../apiConfig';

export class BaseRetrieveService<RetrievePaginatedResponse, RetrieveResponse> extends RetrieveService {
    protected currentPage: number = FIRST_PAGE;
    protected perPage: number = RECORDS_PER_PAGE;
    private httpClient: HttpClient;
    protected apiUrl: string;

    constructor(apiUrl: string, httpClient: HttpClient) {
        super();
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    protected getApiUrl(): string {
        return this.apiUrl;
    }

    protected getHttpClient(): HttpClient {
        return this.httpClient;
    }

    async getEntities(options: { search?: string | null; paginated?: boolean } = {}): Promise<RetrievePaginatedResponse> {
        const baseOptions = {
            search: options.search ?? null,
            paginated: options.paginated ?? true,
        };

        try {
            const { page, perPage } = this.getPagination();

            const mergedOptions = {
                ...baseOptions,
                ...options,
            };
            const params: Record<string, any> = {
                ...mergedOptions,
                page: baseOptions.paginated ? page : undefined,
                per_page: baseOptions.paginated ? perPage : undefined,
            };
            const httpClient = this.getHttpClient();
            const response = await httpClient.get<RetrievePaginatedResponse>(
                this.getApiUrl(), { params });

            if (response.status === StatusCodes.OK) {
                return response.data;
            } else {
                throw new Error(`Unexpected status code: ${response.status} when performing GET operation`);
            }
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error retrieving entities');
        }
    }

    async getEntity(id: number): Promise<RetrieveResponse> {
        try {
            const httpClient = this.getHttpClient();
            const url = `${this.getApiUrl()}/${id}`;
            const response = await httpClient.get<RetrieveResponse>(url);
            if (response.status === StatusCodes.OK) {
                return response.data;
            } else {
                throw new Error(`Unexpected status code: ${response.status}`);
            }
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error retrieving the entity');
        }
    }

    async getEntityWithPath(url: string): Promise<RetrieveResponse> {
        try {
            const httpClient = this.getHttpClient();
            const response = await httpClient.get<RetrieveResponse>(url);
            if (response.status === StatusCodes.OK) {
                return response.data;
            } else {
                throw new Error(`Unexpected status code: ${response.status}`);
            }
        } catch (exception: any) {
            console.log(exception);
            return this.handleApiError(exception, 'Error retrieving the entity on getEntityWithPath');
        }
    }

    setPagination(page: number, perPage: number) {
        this.currentPage = page;
        this.perPage = perPage;
    }

    getPagination() {
        return { page: this.currentPage, perPage: this.perPage };
    }

    getCurrentPage(): number {
        return this.currentPage;
    }
}

import { HttpClient } from '../httpClient';
import { CreateService } from '../abstract/createService';
import { StatusCodes } from 'http-status-codes';
import { handleApiErrorService } from '../../utils/messages';

export class BaseCreateService<CreateResponse> extends CreateService<CreateResponse> {
    protected apiUrl: string;
    private httpClient: HttpClient;

    constructor(apiUrl: string, httpClient: HttpClient) {
        super();
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    protected getApiUrl(): string {
        return this.apiUrl;
    }

    protected getHttpClient(): HttpClient {
        return this.httpClient;
    }

    async createEntity(data: any): Promise<CreateResponse> {
        try {
            const response = await this.httpClient.post<CreateResponse>(this.apiUrl, data);
            if (response.status === StatusCodes.CREATED) {
                return response.data;
            } else {
                throw new Error(`Unexpected status code: ${response.status}`);
            }
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error creating the entity');
        }
    }

    protected handleApiError(exception: any, message: string): any {
        return handleApiErrorService(exception, message);
    }
}

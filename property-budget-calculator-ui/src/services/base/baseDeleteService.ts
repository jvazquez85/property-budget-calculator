import {StatusCodes} from 'http-status-codes';
import {DeleteService} from '../abstract/deleteService';
import {HttpClient} from '../httpClient';
import {handleApiErrorService} from "../../utils/messages.ts";
import {ApiDeleteResponse, ApiError} from "../../interfaces/ApiResponse.ts";

export class BaseDeleteService extends DeleteService {
    private httpClient: HttpClient;
    protected apiUrl: string;

    constructor(apiUrl: string, httpClient: HttpClient) {
        super();
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    protected getApiUrl(): string {
        return this.apiUrl;
    }

    protected getHttpClient(): HttpClient {
        return this.httpClient;
    }

    async deleteEntity(id: number): Promise<ApiDeleteResponse> {
        try {
            const httpClient = this.getHttpClient();
            const url = `${this.getApiUrl()}/${id}`;
            const response = await httpClient.delete<void>(url);
            if (response.status !== StatusCodes.NO_CONTENT) {
                throw new Error(`Unexpected status code: ${response.status} on delete entity`);
            }
            return {
                message: 'Entity deleted successfully',
                success: true,
            };
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error deleting the entity');
        }
    }

    protected handleApiError(exception: any, message: string): ApiError {
        return handleApiErrorService(exception, message);
    }
}

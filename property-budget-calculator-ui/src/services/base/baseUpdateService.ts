import { StatusCodes } from 'http-status-codes';
import { UpdateService } from '../abstract/updateService';
import { HttpClient } from '../httpClient';
import {handleApiErrorService} from "../../utils/messages.ts";

export class BaseUpdateService<UpdateResponse> extends UpdateService {
    private httpClient: HttpClient;
    protected apiUrl: string;

    constructor(apiUrl: string, httpClient: HttpClient) {
        super();
        this.apiUrl = apiUrl;
        this.httpClient = httpClient;
    }

    protected getApiUrl(): string {
        return this.apiUrl;
    }

    protected getHttpClient(): HttpClient {
        return this.httpClient;
    }

    async updateEntity(data: any, id: number): Promise<UpdateResponse> {
        try {
            const httpClient = this.getHttpClient();
            const url = `${this.getApiUrl()}${id}`;
            const response = await httpClient.put<UpdateResponse>(url, data);

            if (response.status === StatusCodes.OK) {
                return response.data;
            } else {
                throw new Error(`Unexpected status code: ${response.status}`);
            }
        } catch (exception: any) {
            return this.handleApiError(exception, 'Error updating the entity');
        }
    }

    protected handleApiError(exception: any, message: string): any {
        return handleApiErrorService(exception, message);
    }
}

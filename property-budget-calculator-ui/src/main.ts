import { createApp } from "vue"
import "bootstrap/dist/css/bootstrap.css";
import "./style.css";
import "bootstrap";
import App from "./App.vue"
import router from "./router"

const app = createApp(App);
app.use(router);  // Use the router instance
app.mount("#app");

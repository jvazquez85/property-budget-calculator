import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
import Calculator from "./components/Calculator.vue";
import HomePlaceHolder from "./components/HomePlaceHolder.vue";
console.log('Router file loaded');
const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: HomePlaceHolder,
    },
    {
        path: '/calcular',
        component: Calculator,
    },
    // {
    //     path: '/offers/:id',
    //     name: 'Offer details',
    //     component: ListOfferDetails,
    //     props: (route: RouteLocationNormalized) => ({
    //         id: route.params.id,
    //     }),
    // },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;

import {Ref} from "vue";
import axios from "axios";
import {ApiResponse} from "../interfaces/ApiResponse.ts";
import {GenericError} from "../services/genericError";

export function handleApiError(
    error: any, // You can specify the error type as needed
    errorMessage: Ref<ApiResponse | null>,
    customErrorMessage: string = 'An error occurred.'
) {
    console.error('Error captured while trying to perform an operation:', error);

    if (axios.isAxiosError(error) && error.response) {
        errorMessage.value = { message: error.response.data.message } as ApiResponse;
    } else {
        errorMessage.value = { message: customErrorMessage } as ApiResponse;
    }
}

export function handleApiErrorService(exception: GenericError, customErrorMessage: string): ApiResponse {
    if (exception.response && exception.response.data && 'message' in (exception.response.data as any)) {
        const theError: string = (exception.response.data as any).message;
        const success: boolean = (exception.response.data as any).success;
        return {
            message: theError,
            errors: theError,
            success: success
        };
    } else {
        return {
            message: `Unhandled exception...${exception.message} ${customErrorMessage}`,
            errors: "",
            success: false
        };
    }
}

// Define a generic function to handle API responses
export const displaySuccessOrErrorMessages = <T extends ApiResponse>(
    response: T,
    successMessageRef: Ref<string>,
    errorMessageRef: Ref<string>,
    displaySuccessMessageRef: Ref<boolean>,
    displayErrorMessageRef: Ref<boolean>,
    timeout: number =5000
) => {
    const theMessage = response.message
    if ('errors' in response) {
        handleErrorDisplay(theMessage, errorMessageRef, displayErrorMessageRef, timeout,'Generic error')
    } else {
        handleSuccessResponse(theMessage, successMessageRef, displaySuccessMessageRef, timeout, 'Generic success!')
    }
};

// Function to handle error response
const handleErrorDisplay = (
    errorMessage: string,
    errorMessageRef: Ref<string>,
    showErrorMessageRef: Ref<boolean>,
    displayFor: number,
    defaultErrorMessage: string = 'Generic error.'
) => {
    if(errorMessage!=""){
        errorMessageRef.value =  errorMessage
    } else {
        errorMessageRef.value =  defaultErrorMessage
    }
    showErrorMessageRef.value = true;
    setTimeout(() => {
        showErrorMessageRef.value = false;
        errorMessageRef.value = '';
    }, displayFor);
};

// Function to handle success response
const handleSuccessResponse = (
    successMessage: string,
    successMessageRef: Ref<string>,
    showSuccessMessageRef: Ref<boolean>,
    displayFor: number,
    defaultSuccessMessage: string
) => {
    if(successMessage!=""){
        successMessageRef.value = successMessage
    } else {
        successMessageRef.value = defaultSuccessMessage
    }
    showSuccessMessageRef.value = true;
    setTimeout(() => {
        showSuccessMessageRef.value = false;
        successMessageRef.value = '';
    }, displayFor);
};
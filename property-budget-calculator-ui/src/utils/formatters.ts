// @ts-ignore
import { DateTimeFormatOptions } from 'intl';

export function formatProbability(probability: number): string {
    return (probability * 100).toFixed(2) + '%';
}

export function formatTimestamp(timestamp: string): string {
    const date = new Date(timestamp);
    const options: Intl.DateTimeFormatOptions = {
        weekday: 'long',
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: 'numeric',
        minute: 'numeric'
    };
    const formattedDate = date.toLocaleString('es-AR', options);
    const weekday = formattedDate.split(',')[0];
    const capitalizedWeekday = weekday.charAt(0).toUpperCase() + weekday.slice(1);

    // Get the rest of the formatted date string without the weekday
    const restOfDateString = formattedDate.split(',').slice(1).join(' ');

    return capitalizedWeekday + restOfDateString;
}
export function formatOfferDate(dateValue: Date){
    const options: Intl.DateTimeFormatOptions = {
        year: 'numeric',
        month: 'long',
        weekday: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
    };
    return dateValue.toLocaleString(undefined, options);
}
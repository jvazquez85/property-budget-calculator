let API_BASE_URL: string;

// Check if the VITE_VUE_APP_API_BASE_URL environment variable is defined
if ((globalThis as any).importMetaEnv?.VITE_VUE_APP_API_BASE_URL) {
    // If it's defined, use the value from the environment variable
    API_BASE_URL = (globalThis as any).importMetaEnv.VITE_VUE_APP_API_BASE_URL;
} else if (window.location.hostname === 'localhost') {
    API_BASE_URL = 'http://localhost:5001';
} else {
    // Use a default value for other environments (e.g., production)
    API_BASE_URL = 'https://24.144.85.99'; // Replace with your actual production URL
}

// function getOfferDetailsUrl(offerId: number) {
//     return `${API_BASE_URL}/api/offers/${offerId}`;
// }
//
// function getRecruiterDetailsUrl(recruiterId: number) {
//     return `${API_BASE_URL}/api/recruiters/${recruiterId}`;
// }

const apiConfig = {
    API_BASE_URL,
    API_CALCULATE: `${API_BASE_URL}/api/calculate`,
    API_LOG: `${API_BASE_URL}/api/vue-log`,
    // API_OFFER_DETAILS: getOfferDetailsUrl,
};

export const RECORDS_PER_PAGE = 10;
export const FIRST_PAGE = 1;
export default apiConfig;

export interface ApiResponse {
    message: string;
    success: boolean;
    errors: string | null
}

export interface Response {
    message: string;
    statusCode: number;
    success: boolean;
}
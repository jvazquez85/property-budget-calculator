import {ApiResponse} from "./ApiResponse";

export interface BudgetCalculatorWriteRequest {
    propertyPrice: number;
    notaryRange: number[];
    auctioneerRate: number;
}

export interface BudgetCalculatorResponse extends ApiResponse {
    propertyPrice: number;
    notaryPriceRange: { [percentage: number]: number };
    auctioneerPriceHalved: number;
    auctioneerPriceGross: number;
}

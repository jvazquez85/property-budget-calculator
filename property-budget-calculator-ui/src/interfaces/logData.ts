export interface LogData {
    level: 'debug' | 'info' | 'warning' | 'error' | 'critical';
    message: string;
    extra?: Record<string, any>;
}
import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    preset: 'ts-jest',
    testEnvironment: 'jest-environment-jsdom',
    transform: {
        '^.+\\.vue$': '@vue/vue3-jest',
        '^.+\\.(ts|tsx)$': ['ts-jest', {
            tsconfig: 'tsconfig.jest.json',
            diagnostics: {
                warnOnly: true
            }
        }],
        '^.+\\.(js|jsx)$': 'babel-jest'
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'json', 'vue'],
    testMatch: [
        '**/tests/**/*.test.ts',
        '**/?(*.)+(spec|test).ts'
    ],
    transformIgnorePatterns: [
        '/node_modules/'
    ],
    moduleNameMapper: {
        '^axios$': '<rootDir>/tests/__mocks__/axios.ts',
        '^@/(.*)$': '<rootDir>/src/$1'  // Alias for @ to src directory
    }
};

export default config;

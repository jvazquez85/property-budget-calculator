import { AxiosInstance } from 'axios';
import mockAxios from 'jest-mock-axios';

const mockAxiosInstance: jest.Mocked<AxiosInstance> = {
    create: jest.fn(() => mockAxiosInstance),
    get: jest.fn(),
    post: jest.fn(),
    put: jest.fn(),
    delete: jest.fn(),
    head: jest.fn(),
    options: jest.fn(),
    patch: jest.fn(),
    request: jest.fn(),
    getUri: jest.fn(),
    interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() }
    },
    defaults: {
        headers: {
            common: {},
            delete: {},
            get: {},
            head: {},
            post: {},
            put: {},
            patch: {}
        }
    }
} as unknown as jest.Mocked<AxiosInstance>;

export default mockAxiosInstance;

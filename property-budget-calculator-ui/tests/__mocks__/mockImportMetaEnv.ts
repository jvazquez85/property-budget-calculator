export const mockImportMetaEnv = {
    VITE_VUE_APP_API_BASE_URL: 'http://localhost:3000',
    VITE_VUE_APP_API_TASKS_URL: 'http://localhost:3000/tasks',
};

import apiConfig from "../../src/apiConfig";
import { createService } from "../../src/services/factoryCreateService";
import { BudgetCalculatorResponse } from "../../src/interfaces/BudgetCalculatorResponse";
import { BudgetCalculatorCreate } from "../../src/services/concrete/budgetCalculatorCreate";

describe('createService function', () => {
    it('should create a service instance without errors', async () => {
        console.log('Starting createService test');
        try {
            const service = createService<BudgetCalculatorResponse, BudgetCalculatorCreate<BudgetCalculatorResponse>>(
                BudgetCalculatorCreate,
                apiConfig.API_CALCULATE
            );
            console.log('Service created:', service);
            expect(service).toBeDefined();
        } catch (error) {
            console.error('Error creating service:', error);
            throw error; // Rethrow the error to fail the test
        }
    });
});
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

export function createMockAxiosError(
    message: string,
    config: AxiosRequestConfig,
    response?: AxiosResponse
): AxiosError {
    return {
        isAxiosError: true,
        toJSON: jest.fn(),
        name: 'AxiosError',
        message,
        config: {
            ...config,
            headers: config.headers || {},
        },
        response,
        request: {},
        code: undefined
    } as AxiosError;
}


FROM golang:1.22

ARG build_user_id
ARG build_group_id
ARG build_username
ARG goprivate
ARG gonoproxy
ARG gonosumdb
ARG gitlab_token
ARG gitlab_user
# Set environment variables based on build arguments
ENV BUILD_USER_ID=$build_user_id
ENV BUILD_GROUP_ID=$build_group_id
ENV BUILD_USERNAME=$build_username
ENV GOPRIVATE=$goprivate
ENV GONOPROXY=$gonoproxy
ENV GONOSUMDB=$gonosumdb
ENV GITLAB_TOKEN=$gitlab_token
ENV GITLAB_USER=$gitlab_user
# Install build dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpcre3 \
    libpcre3-dev \
    libffi-dev \
    libssl-dev

WORKDIR /property-budget-calculator
RUN groupadd -g $BUILD_GROUP_ID offers \
    && useradd -u $BUILD_USER_ID -g $BUILD_GROUP_ID -m -d /home/offers offers
COPY go.mod /property-budget-calculator/
COPY go.sum /property-budget-calculator/
RUN chown -R offers:offers /property-budget-calculator && chown -R offers:offers /home/offers
ENV PATH="/home/offers/.local/bin:${PATH}"
USER offers
COPY .netrc /home/offers/
RUN git config --global credential.helper 'cache --timeout=3600'
RUN git config --global credential.https://gitlab.com.username $GITLAB_USER
RUN git config --global credential.https://gitlab.com.password $GITLAB_TOKEN
RUN go mod download

COPY app /property-budget-calculator/app

RUN GOOS=linux go build -o property-budget-calculator ./app/cmd
CMD ["./property-budget-calculator"]
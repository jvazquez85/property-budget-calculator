package api

import (
	"encoding/json"
	"gitlab.com/jvazquez85/golang-api-shared/shared"
	"log"
	"net/http"
	"property-budget-calculator/app/dto"
)

type App struct {
}

const DefaultPage = 1
const DefaultRecordsPerPage = 10

// writeJSONResponse handles the last response you provide in your code, it's
// used to respond the errors
func writeJSONResponse(w http.ResponseWriter, data interface{}, statusCode int) {
	w.WriteHeader(statusCode)
	jsonResponse, err := json.Marshal(data)
	if err != nil {
		log.Printf("Error marshaling JSON response: %v", err)
		http.Error(w, "Failed to marshal JSON", http.StatusInternalServerError)
		return
	}

	if _, err := w.Write(jsonResponse); err != nil {
		shared.ErrorLogger.Printf("Error writing JSON response: %v", err)
		http.Error(w, "Failed to write JSON response", http.StatusInternalServerError)
		return
	}
}

// StatusHandler handles requests to check if the app is running
func (a *App) StatusHandler(w http.ResponseWriter, _ *http.Request) {
	response := dto.NewResponse("Server is alive", true, http.StatusOK)
	writeJSONResponse(w, response, http.StatusOK)
}

func (a *App) CalculateHandler(w http.ResponseWriter, r *http.Request) {
	var calculateRequest dto.CalculateRequest
	var calculateResponse dto.CalculateResponse
	err := json.NewDecoder(r.Body).Decode(&calculateRequest)
	if err != nil {
		shared.InfoLogger.Printf("Error unmarshaling JSON request: %v", err)
		writeJSONResponse(w, dto.NewResponse("Failed to process your data on /publisher/login", false, http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	calculateResponse = dto.CalculateBudget(calculateRequest)
	writeJSONResponse(w, calculateResponse, http.StatusCreated)
}

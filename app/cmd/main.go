package main

import (
	"flag"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/jvazquez85/golang-api-shared/shared"
	middleware "gitlab.com/jvazquez85/golang-api-shared/shared/middleware"
	"log"
	"net/http"
	"os"
	"property-budget-calculator/app/api"
)

const DefaultHostAndPort = ":5000"

func main() {
	logLevel := flag.String("logLevel", "info", "Configures the logLevel of the app")
	flag.Parse()
	shared.SetLogLevel(*logLevel)
	shared.InfoLogger.Print("Starting job-offers-api")
	envFileLocal := os.Getenv("ENV_FILE")
	if envFileLocal != "" {
		shared.DebugLogger.Printf("Using a local env file %s", envFileLocal)
		if err := godotenv.Load(envFileLocal); err != nil {
			shared.ErrorLogger.Fatalf("Error loading .env file\n%+v\n", err)
		}
	}
	app := &api.App{}
	router, hostAndPort := initializeRouter(app)
	shared.InfoLogger.Printf("Property budget calculator running on %s", hostAndPort)
	err := http.ListenAndServe(hostAndPort, router)
	if err != nil {
		log.Fatalf("We couldn't serve the app, got error %v", err)
	}
}

/*
initializeRouter prepares the gorilla mux router and we register
all the routes that we serve. This function also returns the hostAndPort we use
*/
func initializeRouter(app *api.App) (*mux.Router, string) {
	r := mux.NewRouter()
	r.Use(middleware.LoggingMiddleware)
	r.Use(middleware.CorsMiddleware)
	//r.Use(middleware.JWTMiddleware)
	r.Use(middleware.JSONMiddleware)
	r.Use(middleware.RecoverMiddleware)

	hostAndPort := os.Getenv("GO_API_PORT")
	if hostAndPort == "" {
		hostAndPort = DefaultHostAndPort
	}
	r.HandleFunc("/api/status", app.StatusHandler).Methods("GET", "OPTIONS")
	r.HandleFunc("/api/calculate", app.CalculateHandler).Methods("POST", "OPTIONS")
	//r.HandleFunc("/api/login", app.LoginHandler).Methods("POST", "OPTIONS")
	//r.HandleFunc("/api/tasks", app.DispatchTasksHandler).Methods("POST", "OPTIONS")
	//r.HandleFunc("/api/remote-ok/{ID}/summary", app.SummaryHandler).Methods("GET", "OPTIONS")
	//r.HandleFunc("/api/remote-ok", app.RemoteOkJobs).Methods("GET", "OPTIONS")
	//r.HandleFunc("/api/remote-ok/{ID}", app.GetRemoteOkJobById).Methods("GET", "OPTIONS")
	return r, hostAndPort
}

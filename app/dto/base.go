package dto

type Response struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
	Success    bool   `json:"success"`
}

func NewResponse(message string, success bool, statusCode int) Response {
	return Response{
		Message:    message,
		StatusCode: statusCode,
		Success:    success,
	}
}

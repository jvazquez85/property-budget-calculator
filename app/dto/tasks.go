package dto

type Task struct {
	Name    string `json:"name"`
	Payload string `json:"payload"`
}

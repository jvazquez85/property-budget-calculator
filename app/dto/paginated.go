package dto

type Paginated struct {
	Page    int    `json:"page"`
	PerPage int    `json:"per_page"`
	Search  string `json:"search"`
}

func NewPaginatedResponse(message string) Paginated {
	return Paginated{
		Page:    1,
		PerPage: 10,
		Search:  "",
	}
}

package dto

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCalculateBudget(t *testing.T) {
	tests := []struct {
		name     string
		request  CalculateRequest
		expected CalculateResponse
	}{
		{
			name: "Default notary range and auctioneer rate",
			request: CalculateRequest{
				PropertyPrice:  71000,
				NotaryRange:    nil,
				AuctioneerRate: 0,
			},
			expected: CalculateResponse{
				Response: Response{
					Message:    "Budget calculation successful",
					StatusCode: 201,
					Success:    true,
				},
				PropertyPrice: 71000,
				NotaryPriceRange: map[string]float64{
					"7.00":  2485.00,
					"8.00":  2840.00,
					"9.00":  3195.00,
					"10.00": 3550.00,
				},
				AuctioneerPriceHalved: 1420.00,
				AuctioneerPriceGross:  2840.00,
			},
		},
		{
			name: "Provided notary range and auctioneer rate",
			request: CalculateRequest{
				PropertyPrice:  71000,
				NotaryRange:    []float64{7.5},
				AuctioneerRate: 5,
			},
			expected: CalculateResponse{
				Response: Response{
					Message:    "Budget calculation successful",
					StatusCode: 201,
					Success:    true,
				},
				PropertyPrice: 71000,
				NotaryPriceRange: map[string]float64{
					"7.50": 2662.50,
				},
				AuctioneerPriceHalved: 1775.00,
				AuctioneerPriceGross:  3550.00,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := CalculateBudget(tt.request)
			assert.Equal(t, tt.expected.PropertyPrice, actual.PropertyPrice)
			assert.Equal(t, tt.expected.Response, actual.Response)
			assert.InDeltaMapValues(t, tt.expected.NotaryPriceRange, actual.NotaryPriceRange, 0.01)
			assert.InDelta(t, tt.expected.AuctioneerPriceHalved, actual.AuctioneerPriceHalved, 0.01)
			assert.InDelta(t, tt.expected.AuctioneerPriceGross, actual.AuctioneerPriceGross, 0.01)
		})
	}
}

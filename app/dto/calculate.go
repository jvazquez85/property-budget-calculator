package dto

import (
	"fmt"
	"net/http"
	"sort"
)

type CalculateRequest struct {
	PropertyPrice  float64   `json:"propertyPrice"`
	NotaryRange    []float64 `json:"notaryRange"`
	AuctioneerRate float64   `json:"auctioneerRate"`
}

type CalculateResponse struct {
	Response
	PropertyPrice         float64            `json:"propertyPrice"`
	NotaryPriceRange      map[string]float64 `json:"notaryPriceRange"`
	AuctioneerPriceHalved float64            `json:"auctioneerPriceHalved"`
	AuctioneerPriceGross  float64            `json:"auctioneerPriceGross"`
}

/*
CalculateBudget Calculate the estimateI'm using the defaults I've seen over the last contacts I had with different auctioneers
Usually the property price is halved when it is put in papers, so that explains the division over 2,
but this factor may change in the future, since it could be parametrized if
the person purchasing the property knows ahead of time this.
The estimates I've been told by auctioneers for the notary percentage over the property goes from 7 to 10, but that
may be corrected after I purchase.
The same applies to the auctioneer rate, most of them have 4% by default, some others have told me 3.5%
*/
func CalculateBudget(request CalculateRequest) CalculateResponse {
	halfPrice := request.PropertyPrice / 2

	// Use provided notary range or default to [7, 8, 9, 10]
	notaryRange := request.NotaryRange
	if len(notaryRange) == 0 {
		notaryRange = []float64{7, 8, 9, 10}
	}

	// Sort the notary range percentages
	sort.Float64s(notaryRange)

	// Create a sorted map for notary prices
	notaryPriceRange := make(map[string]float64)
	for _, percentage := range notaryRange {
		key := fmt.Sprintf("%.2f", percentage)
		notaryPriceRange[key] = (percentage / 100.0) * halfPrice
	}

	// Use provided auctioneer rate or default to 4%
	auctioneerRate := request.AuctioneerRate
	if auctioneerRate == 0 {
		auctioneerRate = 4
	}
	auctioneerPriceHalved := (auctioneerRate / 100.0) * halfPrice
	auctioneerPriceGross := (auctioneerRate / 100.0) * request.PropertyPrice

	response := CalculateResponse{
		Response: Response{
			Message:    "Budget calculation successful",
			StatusCode: http.StatusCreated,
			Success:    true,
		},
		PropertyPrice:         request.PropertyPrice,
		NotaryPriceRange:      notaryPriceRange,
		AuctioneerPriceHalved: auctioneerPriceHalved,
		AuctioneerPriceGross:  auctioneerPriceGross,
	}

	return response
}

package dto

import "net/http"

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginErrorResponse struct {
	Response
}

type LoginSuccessResponse struct {
	AccessToken string `json:"access_token"`
}

func NewLoginErrorResponseSchema(message string) LoginErrorResponse {
	return LoginErrorResponse{
		Response: NewResponse(message, false, http.StatusBadRequest), // Set default ErrorResponse values
	}
}
